let trainer = {
	name: "Blue",
	talk: function(){
		console.log(this.name + ": Pikachu! I choose you!");
	}
};

let friend = {
	firstName: "Jessie",
	lastName: "Rocket",
	address: {
		city: "Pallett",
		state: "Town"
	},
	emails: ["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + "" + this.lastName)
	}
};



let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
};

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level

	// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	},
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
};

let Pikachu = new Pokemon("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 36);
let Bulbasaur = new Pokemon("Charizard", 9);