/*
	log in the console the following gwa per subject of a student:
	98.5
	94.3
	89.2
	90.0
*/

let grades = [98.5, 94.3, 89.2, 90.0];

console.log(grades);
console.log(grades[3]);

/*
	Objects
		collection of related data and functionalities; usually representing real-world objects
*/

let grade = {
	// curly braces - initializer for creating objects
	// ket-value pair
		/*
			key - field / identifier to describe the data/value - information that serves tas the value of the key/field
		*/

	math: 98.5,
	english: 94.3,
	science: 90.0
};

console.log(grade);
/*
	Accessing elements inside the object
		Syntax for creating an object
			let objectName = {
				keyA: valueA,
				keyB: valueB,
			}
	Dot Notation - used to access the specific property/value of the object; preferred in terms of object access
*/

console.log(grade.english);
// accessing two keys in the object
console.log(grade.english, grade.math);


/*
	create a cellphone object that has the following keys (supply your own values for each)
		brand
		color
		mfd
*/

let cellphoneObject = {
	brand: 'ASUS ROG',
	color: 'black',
	mfd: 5010
};

console.log(cellphoneObject);
// typeof determines the the data type
console.log(typeof cellphoneObject);

let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 09175456259,
	location: {
		city: "Tokyo",
		country: "Japan",
	},
	email: ["john@mail.com", "john.smith@mail.com"],

	fullName: (function name(fullName){
		// this - refers to the object where it is inserted
		console.log(this.firstName + " " + this.lastName);
	})
};

console.log(student.firstName + " " + student.lastName);
student.fullName();

console.log(student.location.city);

console.log(student.email);
console.log(student.email[0]);
console.log(student.email[1]);

/*
	Array of objects
*/

let contactList = [
	{
		firstName: "John",
		lastName: "Smith",
		location: "Japan"
	},
	{
		firstName: "Jane",
		lastName: "Smith",
		location: "Japan"
	},
	{
		firstName: "Jasmine",
		lastName: "Smith",
		location: "Japan"
	}
];

// accessing an element inside the array
console.log(contactList[1]);
// accessing a key in an object that is inside the array
console.log(contactList[1].firstName);

/*
	Constructor function - create a reusable function to create several objects
		-useful for creating copies / instances of an object
*/

/*
	object literals - let objectName = {}

	instances - concrete occurrence of any object which emphasizes on the unique identity of it
*/

// Reusable Function / Object Method
function Laptop(name, manufacturedDate){
	this.name = name,
	this.manufacturedDate = manufacturedDate
};

let laptop1 = new Laptop("Lenovo", 2008);
console.log(laptop1);

let laptop2 = new Laptop("Toshiba", 1997);
console.log(laptop2);

let laptops = [laptop1, laptop2];
console.log(laptops);

let car = {};
console.log(car);

// adding object properties
car.name = "Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020;
console.log(car);

// reassigning object (same / existing properties, but assigning a different value)
// car["name"] = "Volvo";
car.name = "Volvo";
console.log(car);

// deleting object properties
delete car.manufacturedDate;
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log("Hi! My name is" + this.name)
	}
};

// add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
};


let friend = {
	firstName: "Joe",
	lastName: "Doe",
	address: {
		city: "Austin",
		state: "Texas"
	},
	emails: ["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + "" + this.lastName)
	}
};



let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
};

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level

	// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	},
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
};

let Pikachu = new Pokemon("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);
let Bulbasaur = new Pokemon("Charizard", 9);


